console.log('I loaded js!');


$('.hamburger').click(function() {
    $('nav').toggleClass('open');
});   



$('.answer').slideUp(0);

$('.qa').click(function() {

    var clickedAnswer= $(this).find('.answer');

    $('.answer').not(clickedAnswer).slideUp();

    clickedAnswer.slideToggle();


    var clickedArrow = $(this).find('.question span')

    $('.question span').not (clickedArrow).removeClass('open');
    clickedArrow.toggleClass('open');
});



$('.myCarousel').slick({
    slidesToShow: 1,
    autoplay: true,
    autplaySpeed: 7000,
    dots: true,
    prevArrow: '<div class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>',
    nextArrow: '<div class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>',
});

